use master
go
IF NOT EXISTS(SELECT name FROM master.dbo.sysdatabases WHERE NAME = 'baseNewsH')
create database baseNewsH

go

use baseNewsH

go 

if not exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'histories')
create table histories(
idH int primary key identity(1,1),
city varchar(60),
info varchar(60),
)

