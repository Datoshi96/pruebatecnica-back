
go
use baseNewsH
go

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'p_register')
DROP PROCEDURE p_register

go

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'p_update')
DROP PROCEDURE p_update

go

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'p_get_by_id')
DROP PROCEDURE p_get_by_id

go

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'p_get')
DROP PROCEDURE p_get

go

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'p_delete')
DROP PROCEDURE p_delete

go

create procedure p_register_info(
@city varchar(60),
@info varchar(60)
)
as
begin

insert into histories(city,info)
values
(
@city,
@info
)
end

go
create procedure p_update(
@idH int,
@city varchar(60),
@info varchar(60)
)
as
begin

update histories set 
city = @city,
info = @info
where idH = @idH

end

go

create procedure p_get_by_id(@idH int)
as
begin

select * from histories where idH = @idH
end

go
create procedure p_get
as
begin

select * from histories
end

go

create procedure p_delete(
@idH int
)
as
begin

delete from histories where idH = @idH

end