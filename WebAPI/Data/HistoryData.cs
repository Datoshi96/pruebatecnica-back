﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class HistoryData
    {
        public static bool Register(History oHistory) {
            using(SqlConnection oConexion = new SqlConnection(Conexion.rutaConexion)){
                SqlCommand cmd = new SqlCommand("p_register_info", oConexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@city", oHistory.city);
                cmd.Parameters.AddWithValue("@info", oHistory.info);

                try
                {
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex) {
                    return false;
                
                }

            }
        }

        public static bool Update(History oHistory)
        {
            using (SqlConnection oConexion = new SqlConnection(Conexion.rutaConexion))
            {
                SqlCommand cmd = new SqlCommand("p_update", oConexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idH", oHistory.idH);
                cmd.Parameters.AddWithValue("@city", oHistory.city);
                cmd.Parameters.AddWithValue("@info", oHistory.info);

                try
                {
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;

                }
            }
        }
        public static List<History> GetHistories()
        {
            List<History> oListHistory = new List<History>();
            using (SqlConnection oConexion = new SqlConnection(Conexion.rutaConexion))
            {
                SqlCommand cmd = new SqlCommand("p_get", oConexion);
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    oConexion.Open();
                    //cmd.ExecuteNonQuery();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            oListHistory.Add(new History()
                            {
                                idH = Convert.ToInt32(dr["idH"]),
                                city = dr["city"].ToString(),
                                info = dr["info"].ToString(),
                            });
                        }

                    }
                    return oListHistory;
                }
                catch (Exception ex)
                {
                    return oListHistory;
                }
            }
        }

        public static History GetHistory(int idhistory)
        {
            History oHistory = new History();
            using (SqlConnection oConexion = new SqlConnection(Conexion.rutaConexion))
            {
                SqlCommand cmd = new SqlCommand("p_get_by_id", oConexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idusuario", idhistory);
                try
                {
                    oConexion.Open();
                    //cmd.ExecuteNonQuery();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            oHistory = new History()
                            {
                                idH = Convert.ToInt32(dr["idH"]),
                                city = dr["city"].ToString(),
                                info = dr["info"].ToString()
                            };
                        }
                    }
                    return oHistory;
                }
                catch (Exception ex)
                {
                    return oHistory;
                }
            }
        }

        public static bool Delete(int id)
        {
            using (SqlConnection oConexion = new SqlConnection(Conexion.rutaConexion))
            {
                SqlCommand cmd = new SqlCommand("p_delete", oConexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idH", id);

                try
                {
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }


    }
}