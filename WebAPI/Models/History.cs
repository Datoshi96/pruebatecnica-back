﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class History
    {
        public int idH { get; set; }
        public string city { get; set; }
        public string info { get; set; }
    }
}