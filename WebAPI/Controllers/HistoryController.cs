﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class HistoryController : ApiController
    {

        // GET api/<controller>
        public List<History> Get()
        {
            return HistoryData.GetHistories();
        }

        // GET api/<controller>/5
        public History Get(int id)
        {
            return HistoryData.GetHistory(id);
        }

        // POST api/<controller>
        public bool Post([FromBody] History oHistory)
        {
            return HistoryData.Register(oHistory);
        }

        // PUT api/<controller>/5
        public bool Put([FromBody] History oHistory)
        {
            return HistoryData.Update(oHistory);
        }

        // DELETE api/<controller>/5
        public bool Delete(int id)
        {
            return HistoryData.Delete(id);
        }
    }
}